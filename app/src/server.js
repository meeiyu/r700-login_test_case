//Express 3.x
var express = require('express');
var app = express();
var server = require('http').createServer(app)
var io = require('socket.io').listen(server);

var lon = 25.079722717882092;
var lat = 121.57182583739973;

//listen port
server.listen(1234);

//.on()為socket的接收端，預設的key值是connection
io.sockets.on('connection', function (socket) {

    setInterval(function(){
        lon = lon + 0.0001;
        lat = lat - 0.0001;  
       socket.emit('news', {  longitude: lon, latitude: lat });
       console.log("Server emit");
    }, 5000);

    //接收
    socket.on('news', function (news) {
        console.log("Server get");
        console.log(news);
    });

    //接收測試的內容
    socket.on("echo", function (msg, callback) {
        callback = callback || function () {};

        console.log(msg);
        socket.emit("echo", msg);
        callback(null, "Done.");
    });
});