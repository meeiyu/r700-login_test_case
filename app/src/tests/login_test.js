module('Qunit Test', {
    setup: function() {
        console.log("============================== Qunit Test start ==============================");
    },
    teardown: function() {
        // console.log("============================= Qunit TEST teardown ==========================");
    }
});

test('1.4-Check Password format', function() {
    console.log('1.4-Check Password format');
    var data_type = $("#input_pwd").attr('type');
    ok(data_type == "password", "#input_pwd attr equal password");
});

test('1.5-Empty Username', function(details) {
    console.log('1.5-Empty Username');
    $("#input_user").val("");
    $("#input_pwd").val("test");
    $("#loginBtn").trigger("click");
    equal($("#message").text(), "ERROR: The password field is empty.");
    equal($("#message").hasClass("is-hidden"), false);
});

test('1.6-Empty Password', function() {
    console.log('1.6-Empty Password');
    $("#input_user").val("test");
    $("#input_pwd").val("");
    $("#loginBtn").trigger("click");
    equal($("#message").text(), "ERROR: The password field is empty.");
    equal($("#message").hasClass("is-hidden"), false);
});

test('1.7-Empty Username and Password', function() {
    console.log('1.7-Empty Username and Password');
    $("#input_user").val("");
    $("#input_pwd").val("");
    $("#loginBtn").trigger("click");
    equal($("#message").text(), "ERROR: The password field is empty.");
    equal($("#message").hasClass("is-hidden"), false);
});

test('1.8-Wrong Username and Password', function() {
    console.log('1.8-Wrong Username and Password');
    $("#input_user").val("Wrong");
    $("#input_pwd").val("Wrong");
    $("#loginBtn").trigger("click");
    equal($("#message").text(), "ERROR: The password field is empty.");
    equal($("#message").hasClass("is-hidden"), false);
});

test('2.1-Default language', function() {
    console.log('2.1-Default language');
    var Default_lan,
        Current_lan;

    if (typeof localStorage.language_type === "undefined"){
        Default_lan = $("#select_Language").val();
        ok(Default_lan == "en" , "Current language is English");
    } else {
        Current_lan = localStorage.language_type;
        ok(Current_lan == localStorage.language_type, "Current language is " + Current_lan);
    }
});

test('2.2-Change language then change placeholder', function() {
    console.log('2.2-Change language then change placeholder');
    var Current_lan = localStorage.language_type,
        user_placeholder,
        pwd_placeholder;
        
        if (Current_lan == "zh-TW") {
            user_placeholder = $("#input_user").attr('placeholder')
            pwd_placeholder  = $("#input_pwd").attr('placeholder');
            ok(user_placeholder == "帳號" , "input_user placeholder display 帳號");
            ok(pwd_placeholder == "密碼" , "input_pwd placeholder display 密碼");
        } else if (Current_lan == "en"){
            user_placeholder = $("#input_user").attr('placeholder')
            pwd_placeholder  = $("#input_pwd").attr('placeholder');
            ok(user_placeholder == "Username" , "input_user placeholder display Username");
            ok(pwd_placeholder == "Password" , "input_pwd placeholder display Password");
        } 
});

test('2.3-Keep select option', function() {
    console.log('2.3-Keep select option');
    var Current_lan      = localStorage.language_type,
        Current_selected = $('#select_Language option[value="' + Current_lan + '"]').prop("selected");
    equal(Current_selected, true , "Current selected option is " + Current_lan);
});

test('test socket io echos message', function() {
    console.log('test socket io echos message');
    
    var echo_message;
    var client = io.connect("http://192.168.1.64:1234");

    stop();
 
        client.once("connect", function () {
            client.once("echo", function (message) {

                //equal(message, "Hello World");
                echo_message = message;
                
                client.disconnect();
                done();

            });

            client.emit("echo", "Hello World");
        });

        setTimeout(function(){
            equal(echo_message, "Hello World");
            start();
        },3000);
});         

QUnit.log(function(details) {

    if (details.result) {
        return;
    }

    var loc = details.module + ": " + details.name + ": ",
        output = "FAILED: " + loc + ( details.message ? details.message + ", " : "" );
    if (details.actual) {
        output += "expected: " + details.expected + ", actual: " + details.actual;
    }

    if (details.source) {
        output += ", " + details.source;
    }
        console.log(output);

});